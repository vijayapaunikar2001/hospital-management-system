import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Miniproject4';
   // Login
   loginemail="";
   password="";
   selectedIndex="";
   isEditBtnClicked="no";
   loginList:any=[]
 
   submit(){
     let login ={
       loginemail:this.loginemail,
       password:this.password
     }
     this.loginList.push(login)
     this.clear()
     console.log("this.loginList",this.loginList);
   }
 
   clear(){
     this.loginemail="";
     this.password="";
   }
 
   edit(idx:any){
     this.isEditBtnClicked="yes";
     this.selectedIndex = idx;
     this.loginemail=this.loginList[idx].loginemail;
     this.password=this.loginList[idx].password;
 
   }
 
   delete(idx:any){
     this.loginList.splice(idx,1);
   }
 
   update(){
     this.loginList[this.selectedIndex].loginemail = this.loginemail;
     this.loginList[this.selectedIndex].password = this.password;
     this.clear();
     this.isEditBtnClicked="no";
   }
   // Register
   registerList:any=[];
   rname="";
   rcontact="";
   raddress="";
   remail="";
   selectedIndex1="";
   isEditBtnClicked1="no";
 
   submit1(){
     let register ={
       rname:this.rname,
       rcontact:this.rcontact,
       raddress:this.raddress,
       remail:this.remail,
     }
     this.registerList.push(register)
     this.clear1()
     console.log("this.registerList",this.registerList);
   }
 
   clear1(){
     this.rname="";
     this.remail="";
     this.rcontact="";
     this.raddress="";
   }
   edit1(idx:any){
     this.isEditBtnClicked1="yes";
     this.selectedIndex1 = idx;
     this.rname=this.registerList[idx].rname;
     this.remail=this.registerList[idx].remail;
     this.rcontact=this.registerList[idx].rcontact;
     this.raddress=this.registerList[idx].raddress;
   }
 
   delete1(idx:any){
     this.registerList.splice(idx,1);
   }
 
   update1(){
     this.registerList[this.selectedIndex1].rname = this.rname;
     this.registerList[this.selectedIndex1].remail = this.remail;
     this.registerList[this.selectedIndex1].rcontact = this.rcontact;
     this.registerList[this.selectedIndex1].raddress = this.raddress;
     
     this.clear1();
     this.isEditBtnClicked1="no";
   }
 
   // Doctors
   doctorsList:any=[];
   tname="";
   tid="";
   tcontact="";
   taddress="";
   selectedIndex2="";
   isEditBtnClicked2="no";
 
   submit2(){
     let doctor ={
       tname:this.tname,
       tid:this.tid,
       tcontact:this.tcontact,
       taddress:this.taddress,
     }
     this.doctorsList.push(doctor)
     this.clear2()
     
   }
 
   clear2(){
     this.tname="";
     this.tid="";
     this.tcontact="";
     this.taddress="";
   }
   edit2(idx:any){
     this.isEditBtnClicked2="yes";
     this.selectedIndex2 = idx;
     this.tname=this.doctorsList[idx].tname;
     this.tid=this.doctorsList[idx].tid;
     this.taddress=this.doctorsList[idx].taddress;
     this.tcontact=this.doctorsList[idx].tcontact;
   }
  
   delete2(idx:any){
     this.doctorsList.splice(idx,1);
   }
 
   update2(){
     this.doctorsList[this.selectedIndex2].tname = this.tname;
     this.doctorsList[this.selectedIndex2].tid = this.tid;
     this.doctorsList[this.selectedIndex2].tcontact = this.tcontact;
     this.doctorsList[this.selectedIndex2].taddress = this.taddress;
     
     this.clear2();
     this.isEditBtnClicked2="no";
   }
 //Patients
 patientList:any=[];
 sname="";
 sprob="";
 scontact="";
 saddress="";
 selectedIndex3="";
 isEditBtnClicked3="no";
 
 submit3(){
   let patient ={
     sname:this.sname,
     sprob:this.sprob,
     scontact:this.scontact,
     saddress:this.saddress,
   }
   this.patientList.push(patient)
   this.clear3()
   
 }
 
 clear3(){
   this.sname="";
   this.sprob="";
   this.scontact="";
   this.saddress="";
 }
 edit3(idx:any){
   this.isEditBtnClicked3="yes";
   this.selectedIndex3 = idx;
   this.sname=this.patientList[idx].sname;
   this.sprob=this.patientList[idx].sprob;
   this.scontact=this.patientList[idx].scontact;
   this.saddress=this.patientList[idx].saddress;
 }
 
 delete3(idx:any){
   this.patientList.splice(idx,1);
 }
 
 update3(){
   this.patientList[this.selectedIndex3].sname = this.sname;
   this.patientList[this.selectedIndex3].sprob = this.sprob;
   this.patientList[this.selectedIndex3].scontact = this.scontact;
   this.patientList[this.selectedIndex3].saddress = this.saddress;
   
   this.clear3();
   this.isEditBtnClicked3="no";
 }
 // Staff
 staffList:any=[];
   fname="";
   fid="";
   fcontact="";
   faddress="";
   selectedIndex4="";
   isEditBtnClicked4="no";
 
   submit4(){
     let staff ={
       fname:this.fname,
       fid:this.fid,
       fcontact:this.fcontact,
       faddress:this.faddress,
     }
     this.staffList.push(staff)
     this.clear4()
     
   }
 
   clear4(){
     this.fname="";
     this.fid="";
     this.fcontact="";
     this.faddress="";
   }
   edit4(idx:any){
     this.isEditBtnClicked4="yes";
     this.selectedIndex4 = idx;
     this.fname=this.staffList[idx].fname;
     this.fid=this.staffList[idx].fid;
     this.fcontact=this.staffList[idx].fcontact;
     this.faddress=this.staffList[idx].faddress;
   }
 
   delete4(idx:any){
     this.staffList.splice(idx,1);
   }
 
   update4(){
     this.staffList[this.selectedIndex4].fname = this.fname;
     this.staffList[this.selectedIndex4].fid = this.fid;
     this.staffList[this.selectedIndex4].fcontact = this.fcontact;
     this.staffList[this.selectedIndex4].faddress = this.faddress;
     
     this.clear4();
     this.isEditBtnClicked4="no";
   }
   // Billing
   billList:any=[];
   ename="";
   econtact="";
   eprob="";
   efees="";
   selectedIndex5="";
   isEditBtnClicked5="no";
 
   submit5(){
     let bill ={
       ename:this.ename,
       econdact:this.econtact,
       eprob:this.eprob,
       efees:this.efees,
     }
     this.billList.push(bill)
     this.clear5()
     
   }
 
   clear5(){
     this.ename="";
     this.econtact="";
     this.eprob="";
     this.efees="";
   }
   edit5(idx:any){
     this.isEditBtnClicked5="yes";
     this.selectedIndex5 = idx;
     this.ename=this.billList[idx].ename;
     this.econtact=this.billList[idx].econtact;
     this.eprob=this.billList[idx].eprob;
     this.efees=this.billList[idx].efees;
   }
 
   delete5(idx:any){
     this.billList.splice(idx,1);
   }
 
   update5(){
     this.billList[this.selectedIndex5].ename = this.ename;
     this.billList[this.selectedIndex5].econtact = this.econtact;
     this.billList[this.selectedIndex5].eprob = this.eprob;
     this.billList[this.selectedIndex5].efees = this.efees;
     this.clear5();
     this.isEditBtnClicked5="no";
   }
 
 
   
   
}
